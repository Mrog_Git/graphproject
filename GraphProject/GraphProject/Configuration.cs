﻿using System;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using System.Xml.Linq;

namespace GraphProject
{
    public class Configuration
    {
        private const int MinWidth = 595;
        private const int MinHeight = 455;

        private Color _windowColor;
        private int _windowWidth;
        private int _windowHeight;
        private string _formatOfExportShapesFiles;
        private string _formatOfCreatedShapesFiles;

        public Configuration()
        {
            _windowColor = Color.FromKnownColor(KnownColor.Control);
            _windowHeight = MinHeight;
            _windowWidth = MinWidth;
            AllowToCreateShapes = true;
            AllowToExportShapes = true;
            _formatOfCreatedShapesFiles = ".save";
            _formatOfExportShapesFiles = ".dll";
        }

        public string WindowColor
        {
            get { return _windowColor.Name; }
            set
            {
                try
                {
                    var color = Color.FromName(value);
                    if (color.IsKnownColor) _windowColor = color;
                }
                catch (Exception e)
                {
                    MessageBox.Show(@"Configuration error: " + e.Message);
                }
            }
        }

        public int WindowWidth
        {
            get { return _windowWidth; }
            set
            {
                if (value < MinWidth || value > SystemInformation.PrimaryMonitorSize.Width) return;
                _windowWidth = value;
            }
        }

        public int WindowHeight
        {
            get { return _windowHeight; }
            set
            {
                if (value < MinHeight || value > SystemInformation.PrimaryMonitorSize.Height) return;
                _windowHeight = value;
            }
        }

        public bool AllowToExportShapes { get; set; }
        public bool AllowToCreateShapes { get; set; }

        public string FormatOfExportShapesFiles
        {
            get { return _formatOfExportShapesFiles; }
            set
            {
                if (String.IsNullOrEmpty(value) || value[0] != '.') return;
                foreach (var invalidFileNameChar in Path.GetInvalidFileNameChars())
                {
                    if (value.Contains(invalidFileNameChar)) return;

                }
                _formatOfExportShapesFiles = value;
            }
        }

        public string FormatOfCreatedShapesFiles
        {
            get { return _formatOfCreatedShapesFiles; }
            set
            {
                if (String.IsNullOrEmpty(value) || value[0] != '.') return;
                foreach (var invalidFileNameChar in Path.GetInvalidFileNameChars())
                {
                    if (value.Contains(invalidFileNameChar)) return;

                }
                _formatOfCreatedShapesFiles = value;
            }
        }

        public void Configure(MainForm form)
        {
            form.Width = WindowWidth;
            form.Height = WindowHeight;
            form.BackColor = _windowColor;
            form.Controls.Find("btnSaveInstrument", false)[0].Enabled = AllowToCreateShapes;
        }

        public static Configuration LoadConfiguration()
        {
            var cfg = new Configuration();
            try
            {
                var doc = XDocument.Load("config.xml");
                var config = doc.Element("Configurations");
                if (config != null)
                {
                    var windowConfig = config.Element("Window");
                    if (windowConfig != null)
                    {
                        var ww = windowConfig.Attribute(nameof(WindowWidth));
                        var wh = windowConfig.Attribute(nameof(WindowHeight));
                        var wc = windowConfig.Attribute(nameof(WindowColor));
                        if (ww != null) cfg.WindowWidth = Convert.ToInt32(ww.Value);
                        if (wh != null) cfg.WindowHeight = Convert.ToInt32(wh.Value);
                        if (wc != null) cfg.WindowColor = wc.Value;
                    }
                    var shapeConfig = config.Element("Shapes");
                    if (shapeConfig != null)
                    {
                        var cs = shapeConfig.Attribute(nameof(AllowToCreateShapes));
                        var es = shapeConfig.Attribute(nameof(AllowToExportShapes));
                        if (cs != null) cfg.AllowToCreateShapes = bool.Parse(cs.Value);
                        if (es != null) cfg.AllowToExportShapes = bool.Parse(es.Value);
                    }
                    var formatsConfig = config.Element("Formats");
                    if (formatsConfig != null)
                    {
                        var cs = formatsConfig.Attribute(nameof(FormatOfCreatedShapesFiles));
                        var es = formatsConfig.Attribute(nameof(FormatOfExportShapesFiles));
                        if (cs != null) cfg.FormatOfCreatedShapesFiles = cs.Value;
                        if (es != null) cfg.FormatOfExportShapesFiles = es.Value;
                    }
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(@"Configuration error:"+e.Message);
            }


            return cfg;
        }

        public void SaveConfiguration()
        {
            var doc = new XDocument();
            var config = new XElement("Configurations");
            var windowConfig = new XElement("Window");
            windowConfig.Add(new XAttribute(nameof(WindowWidth), WindowWidth.ToString()));
            windowConfig.Add(new XAttribute(nameof(WindowHeight), WindowHeight.ToString()));
            windowConfig.Add(new XAttribute(nameof(WindowColor), WindowColor));
            config.Add(windowConfig);
            var shapeConfig = new XElement("Shapes");
            shapeConfig.Add(new XAttribute(nameof(AllowToCreateShapes), AllowToCreateShapes.ToString()));
            shapeConfig.Add(new XAttribute(nameof(AllowToExportShapes), AllowToExportShapes.ToString()));
            config.Add(shapeConfig);
            var formatsConfig = new XElement("Formats");
            formatsConfig.Add(new XAttribute(nameof(FormatOfCreatedShapesFiles), FormatOfCreatedShapesFiles));
            formatsConfig.Add(new XAttribute(nameof(FormatOfExportShapesFiles), FormatOfExportShapesFiles));
            config.Add(formatsConfig);
            doc.Add(config);
            doc.Save("config.xml");
        }
    }
}
