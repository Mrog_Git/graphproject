﻿using System;
using System.Drawing;
using GraphProject.Interfaces;

namespace GraphProject.ShapePack
{
    [Serializable]
    [ShapeName("Polygon")]
    public class Polygon: ManyDotsShape, ISelectable
    {
        public Polygon()
        {

        }

        public Polygon(Point[] points): base(points)
        {
                
        }

        public override void Draw(Graphics g)
        {
            if (Points.Count > 1)
            {
                if (IsDrawing) g.DrawLines(new Pen(PenColor, 2), Points.ToArray());
                else g.DrawPolygon(new Pen(PenColor, 2), Points.ToArray());
            }
        }

        public bool Selected(Point p)
        {
            bool result = false;
            int j = Points.Count - 1;
            for (int i = 0; i < Points.Count; i++)
            {
                if (Points[i].Y < p.Y && Points[j].Y >= p.Y || Points[j].Y < p.Y && Points[i].Y >= p.Y)
                {
                    if (Points[i].X + (p.Y - Points[i].Y) / (Points[j].Y - Points[i].Y) * (Points[j].X - Points[i].X) < p.X)
                    {
                        result = !result;
                    }
                }
                j = i;
            }
            return result;
        }
    }
}
