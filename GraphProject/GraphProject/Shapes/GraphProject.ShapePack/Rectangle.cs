﻿using System;
using System.Drawing;
using GraphProject.Interfaces;

namespace GraphProject.ShapePack
{
    [Serializable]
    [ShapeName("Rectangle")]
    public class Rectangle: TwoDotsShape, ISelectable
    {
        public Rectangle() { }
        
        public override void Draw(Graphics g)
        {
            g.DrawRectangle(new Pen(PenColor, 2), PtStart.X, PtStart.Y, PtEnd.X - PtStart.X, PtEnd.Y - PtStart.Y);
        }

        public bool Selected(Point p)
        {
            return (p.X>=PtStart.X && p.X<=PtEnd.X && p.Y>=PtStart.Y && p.Y<= PtEnd.Y);
        }
    }
}
