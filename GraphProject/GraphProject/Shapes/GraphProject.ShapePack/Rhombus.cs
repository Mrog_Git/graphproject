﻿using System;
using System.Collections.Generic;
using System.Drawing;
using GraphProject.Interfaces;

namespace GraphProject.ShapePack
{
    [Serializable]
    [ShapeName("Rhombus")]
    public class Rhombus: TwoDotsShape, ISelectable
    {
        private List<Point> _points;

        public Rhombus()
        {
        }

        private void SetPoints()
        {
            _points = new List<Point>();
            _points.Add(new Point((PtStart.X + PtEnd.X) / 2, PtStart.Y));
            _points.Add(new Point(PtEnd.X, (PtStart.Y + PtEnd.Y) / 2));
            _points.Add(new Point((PtStart.X + PtEnd.X) / 2, PtEnd.Y));
            _points.Add(new Point(PtStart.X, (PtStart.Y + PtEnd.Y) / 2));
        }

        public override void Draw(Graphics g)
        {
            SetPoints();
            g.DrawPolygon(new Pen(PenColor, 2), _points.ToArray());
        }

        public bool Selected(Point p)
        {
            SetPoints();
            bool result = false;
            int j = _points.Count - 1;
            for (int i = 0; i < _points.Count; i++)
            {
                if (_points[i].Y < p.Y && _points[j].Y >= p.Y || _points[j].Y < p.Y && _points[i].Y >= p.Y)
                {
                    if (_points[i].X + (p.Y - _points[i].Y) / (_points[j].Y - _points[i].Y) * (_points[j].X - _points[i].X) < p.X)
                    {
                        result = !result;
                    }
                }
                j = i;
            }
            return result;
        }
    }
}
