﻿using System;
using System.Drawing;
using GraphProject.Interfaces;

namespace GraphProject.Ellipse
{
    [Serializable]
    [ShapeName("Ellipse")]
    public class Ellipse: TwoDotsShape, ISelectable
    {
        public Ellipse() { }
        
        public override void Draw( Graphics g)
        {
            g.DrawEllipse(new Pen(PenColor,2),
                PtStart.X,PtStart.Y,PtEnd.X-PtStart.X,PtEnd.Y-PtStart.Y);
        }

        public bool Selected(Point p)
        {
            float a = ((float)(PtEnd.X - PtStart.X)) / 2;
            float b = ((float)(PtEnd.Y - PtStart.Y)) / 2;
            return (Math.Pow(p.X-(PtStart.X+a),2) / (a*a) + Math.Pow(p.Y-(PtStart.Y+b),2) / (b*b) <= 1);
        }
    }
}
