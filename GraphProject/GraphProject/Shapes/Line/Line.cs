﻿using System;
using System.Drawing;
using GraphProject.Interfaces;

namespace GraphProject.Line
{
    [Serializable]
    [ShapeName("Line")]
    public class Line : TwoDotsShape, ISelectable
    { 
        public Point RealStartPt;
        public Point RealEndPt;

        public Line() { }

        public override void SetPoints(Point start, Point end)
        {
            RealStartPt = start;
            RealEndPt = end;

            base.SetPoints(start, end);
        }

        public override void Draw(Graphics g)
        {
            g.DrawLine(new Pen(PenColor, 2), RealStartPt, RealEndPt);
        }

        public override void Move(int x, int y)
        {
            base.Move(x, y);

            RealStartPt = new Point(RealStartPt.X + x, RealStartPt.Y + y);
            RealEndPt = new Point(RealEndPt.X + x, RealEndPt.Y + y);
        }

        public override void Resize(int width, int height)
        {
            base.Resize(width, height);
            if (RealEndPt.X>RealStartPt.X && RealEndPt.Y>RealStartPt.Y)
                RealEndPt = new Point(RealStartPt.X + width, RealStartPt.Y + height);
            else if (RealStartPt.X > RealEndPt.X && RealStartPt.Y > RealEndPt.Y)
                RealStartPt = new Point(RealEndPt.X + width, RealEndPt.Y + height);
            else if (RealEndPt.X > RealStartPt.X && RealStartPt.Y > RealEndPt.Y)
            {
                RealStartPt = new Point(RealStartPt.X, PtStart.Y + height);
                RealEndPt = new Point(PtStart.X + width, RealEndPt.Y);
            }
            else
            {
                RealEndPt = new Point(RealEndPt.X, PtStart.Y + height);
                RealStartPt = new Point(PtStart.X + width, RealStartPt.Y);
            }

        }

        public bool Selected(Point p)
        {
            if (p.X <= PtEnd.X && p.X >= PtStart.X && p.Y >= PtStart.Y && p.Y <= PtEnd.Y)
            {
                return (Math.Abs(((p.X - PtStart.X) * (PtEnd.Y - PtStart.Y))
                - ((PtEnd.X - PtStart.X) * (p.Y - PtStart.Y))) <= 6000);

            }
            return false;
        }
    }
}
