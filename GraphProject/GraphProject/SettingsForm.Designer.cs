﻿namespace GraphProject
{
    partial class SettingsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnOk = new System.Windows.Forms.Button();
            this.tbWindowWidth = new System.Windows.Forms.TextBox();
            this.tbWidthHeight = new System.Windows.Forms.TextBox();
            this.tbEI = new System.Windows.Forms.TextBox();
            this.tbCI = new System.Windows.Forms.TextBox();
            this.cbLoadInstruments = new System.Windows.Forms.CheckBox();
            this.cbCreateInstruments = new System.Windows.Forms.CheckBox();
            this.cbColor = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(129, 17);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(77, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Window Width";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(126, 43);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(80, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Window Height";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(130, 69);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(73, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Window Color";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(73, 92);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(133, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Allow to create instruments";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(44, 112);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(163, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "Allow to load external instruments";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(41, 137);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(165, 13);
            this.label6.TabIndex = 5;
            this.label6.Text = "Format of external instuments files";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(44, 163);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(162, 13);
            this.label7.TabIndex = 6;
            this.label7.Text = "Format of created instruments file";
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(96, 203);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 7;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnOk
            // 
            this.btnOk.Location = new System.Drawing.Point(255, 203);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(75, 23);
            this.btnOk.TabIndex = 8;
            this.btnOk.Text = "Save";
            this.btnOk.UseVisualStyleBackColor = true;
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // tbWindowWidth
            // 
            this.tbWindowWidth.Location = new System.Drawing.Point(209, 14);
            this.tbWindowWidth.Name = "tbWindowWidth";
            this.tbWindowWidth.Size = new System.Drawing.Size(100, 20);
            this.tbWindowWidth.TabIndex = 9;
            // 
            // tbWidthHeight
            // 
            this.tbWidthHeight.Location = new System.Drawing.Point(209, 40);
            this.tbWidthHeight.Name = "tbWidthHeight";
            this.tbWidthHeight.Size = new System.Drawing.Size(100, 20);
            this.tbWidthHeight.TabIndex = 10;
            // 
            // tbEI
            // 
            this.tbEI.Location = new System.Drawing.Point(209, 134);
            this.tbEI.Name = "tbEI";
            this.tbEI.Size = new System.Drawing.Size(100, 20);
            this.tbEI.TabIndex = 11;
            // 
            // tbCI
            // 
            this.tbCI.Location = new System.Drawing.Point(209, 160);
            this.tbCI.Name = "tbCI";
            this.tbCI.Size = new System.Drawing.Size(100, 20);
            this.tbCI.TabIndex = 12;
            // 
            // cbLoadInstruments
            // 
            this.cbLoadInstruments.AutoSize = true;
            this.cbLoadInstruments.Location = new System.Drawing.Point(209, 112);
            this.cbLoadInstruments.Name = "cbLoadInstruments";
            this.cbLoadInstruments.Size = new System.Drawing.Size(15, 14);
            this.cbLoadInstruments.TabIndex = 13;
            this.cbLoadInstruments.UseVisualStyleBackColor = true;
            // 
            // cbCreateInstruments
            // 
            this.cbCreateInstruments.AutoSize = true;
            this.cbCreateInstruments.Location = new System.Drawing.Point(209, 92);
            this.cbCreateInstruments.Name = "cbCreateInstruments";
            this.cbCreateInstruments.Size = new System.Drawing.Size(15, 14);
            this.cbCreateInstruments.TabIndex = 14;
            this.cbCreateInstruments.UseVisualStyleBackColor = true;
            // 
            // cbColor
            // 
            this.cbColor.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbColor.FormattingEnabled = true;
            this.cbColor.Location = new System.Drawing.Point(209, 66);
            this.cbColor.Name = "cbColor";
            this.cbColor.Size = new System.Drawing.Size(121, 21);
            this.cbColor.TabIndex = 15;
            // 
            // SettingsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(425, 242);
            this.Controls.Add(this.cbColor);
            this.Controls.Add(this.cbCreateInstruments);
            this.Controls.Add(this.cbLoadInstruments);
            this.Controls.Add(this.tbCI);
            this.Controls.Add(this.tbEI);
            this.Controls.Add(this.tbWidthHeight);
            this.Controls.Add(this.tbWindowWidth);
            this.Controls.Add(this.btnOk);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.MaximumSize = new System.Drawing.Size(441, 281);
            this.MinimumSize = new System.Drawing.Size(441, 281);
            this.Name = "SettingsForm";
            this.Text = "Settings";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnOk;
        private System.Windows.Forms.TextBox tbWindowWidth;
        private System.Windows.Forms.TextBox tbWidthHeight;
        private System.Windows.Forms.TextBox tbEI;
        private System.Windows.Forms.TextBox tbCI;
        private System.Windows.Forms.CheckBox cbLoadInstruments;
        private System.Windows.Forms.CheckBox cbCreateInstruments;
        private System.Windows.Forms.ComboBox cbColor;
    }
}