﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using GraphProject.Interfaces;
using GraphProject.UserShape;

namespace GraphProject
{
    public static class Drawer
    {
        public static Shape DrawingShape { get; set; }
        public static ShapeList ShapeList { get; set; } = new ShapeList();
        public static Dictionary<Button,UserShapeData> UserShapeButtons { get; set; } = new Dictionary<Button, UserShapeData>();
        
        public static Pen Pen { get; set; }
        public static Point PtStart { get; set; }
        public static Point PtEnd { get; set; }
        public static bool IsDrawing { get; set; }
        public static bool HasTempDot { get; set; }
        public static Dictionary<Button, Type> ShapeButtons {get ; set; } = new Dictionary<Button, Type>();

        public static Shape SelectShape(Point p)
        {
            Shape selectedShape = null;
            foreach (var s in ShapeList)
            {
                if (s is ISelectable && (s as ISelectable).Selected(p)) selectedShape = s; 
            }
            return selectedShape;
        }

        public static void Reset()
        {
            IsDrawing = false;
            HasTempDot = false;
            DrawingShape = null;
        }
    }
}
