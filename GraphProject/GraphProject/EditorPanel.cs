﻿using System;
using System.Drawing;
using System.Windows.Forms;
using GraphProject.Interfaces;

namespace GraphProject
{
    internal sealed class EditorPanel: Panel
    {
        public readonly Shape Shape;
        private Point _mouseDownLocation;
        private Size _oldSize;
        private bool _nearTop;
        private bool _nearBottom;
        private bool _nearLeft;
        private bool _nearRight;
        private readonly bool _initResize;
        private const int Range = 10;

        public EditorPanel(Shape shape)
        {
            BackColor = Color.Transparent;
            Cursor = Cursors.SizeAll;
            BorderStyle = BorderStyle.FixedSingle;
            MouseDown += SaveMouseLocation;
            MouseMove += MouseMoveHndl;
            DoubleBuffered = true;

            Shape = shape;
            _initResize = true;
            int borderRange = 2;
            if (shape is TwoDotsShape)
            {
                Width = (shape as TwoDotsShape).PtEnd.X - (shape as TwoDotsShape).PtStart.X+1+borderRange;
                Height = (shape as TwoDotsShape).PtEnd.Y - (shape as TwoDotsShape).PtStart.Y+1+borderRange;
                Location = new Point((shape as TwoDotsShape).PtStart.X-borderRange/2, (shape as TwoDotsShape).PtStart.Y-borderRange/2);
            }
            else
            {
                Width = (shape as ManyDotsShape).EndPointEquivalent.X - (shape as ManyDotsShape).StartPointEquivalent.X + 1 + borderRange;
                Height = (shape as ManyDotsShape).EndPointEquivalent.Y - (shape as ManyDotsShape).StartPointEquivalent.Y + 1 + borderRange;
                Location = new Point((shape as ManyDotsShape).StartPointEquivalent.X - borderRange/2, (shape as ManyDotsShape).StartPointEquivalent.Y - borderRange/2);
            }
            _initResize = false;
            Shape.Move(-Location.X - 1, -Location.Y - 1);

            CreateResizer();
        }

        public Shape EndEditing()
        {
            Shape.Move(Location.X + 1, Location.Y + 1);
            return Shape;
        }

        private void CreateResizer()
        {
            PictureBox i = new PictureBox();
            i.Cursor = Cursors.SizeNWSE;
            i.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;
            i.SizeMode = PictureBoxSizeMode.AutoSize;

        }

        protected override void OnResize(EventArgs eventargs)
        {
            base.OnResize(eventargs);

            if (!_initResize) Shape.Resize(Width-10, Height-10);
            Invalidate();
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);

            Shape.Draw(e.Graphics);
        }

        private void SaveMouseLocation(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                _mouseDownLocation = e.Location;
                if (Cursor != Cursors.SizeAll) _oldSize = Size;
            }
        }

        private void ChangeCursor(object sender, MouseEventArgs e)
        { 
            Cursor = (_nearTop && _nearLeft) || (_nearBottom && _nearRight) ? Cursors.SizeNWSE :
                (_nearRight && _nearTop) || (_nearLeft && _nearBottom) ? Cursors.SizeNESW :
                (_nearRight || _nearLeft) ? Cursors.SizeWE :
                (_nearTop || _nearBottom) ? Cursors.SizeNS : Cursors.SizeAll;
        }

        private void MouseMoveHndl(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                if (Cursor == Cursors.SizeAll) Moving(sender, e);
                else Resizing(sender, e);
            }
            else
            {
                SetNearEdge(e.Location);
                ChangeCursor(sender, e);
            }
        }

        private void SetNearEdge(Point location)
        {
            _nearTop = (location.Y <= Range);
            _nearBottom = (location.Y >= (Height - Range));
            _nearLeft = (location.X <= Range);
            _nearRight = (location.X >= (Width - Range));
        }

        private void Resizing(object sender, MouseEventArgs e)
        {
            if (_nearLeft)
            {
                if (_nearTop)
                {
                    Width -= (e.X - _mouseDownLocation.X);
                    if (Width > 2 * Range) Left += (e.X - _mouseDownLocation.X);
                    Height -= (e.Y - _mouseDownLocation.Y);
                    if (Height > 2 * Range) Top += (e.Y - _mouseDownLocation.Y);
                }
                else if (_nearBottom)
                {
                    Width -= (e.X - _mouseDownLocation.X);
                    if (Width > 2 * Range) Left += (e.X - _mouseDownLocation.X);
                    Height = (e.Y - _mouseDownLocation.Y)
                             + _oldSize.Height;
                }
                else
                {
                    Width -= (e.X - _mouseDownLocation.X);
                    if (Width > 2 * Range) Left += (e.X - _mouseDownLocation.X);
                }
            }
            else if (_nearRight)
            {
                if (_nearTop)
                {
                    Width = (e.X - _mouseDownLocation.X)
                                    + _oldSize.Width;
                    Height -= (e.Y - _mouseDownLocation.Y);
                    if (Height > 2 * Range) Top += (e.Y - _mouseDownLocation.Y);

                }
                else if (_nearBottom)
                {
                    Width = (e.X - _mouseDownLocation.X)
                                    + _oldSize.Width;
                    Height = (e.Y - _mouseDownLocation.Y)
                                    + _oldSize.Height;
                }
                else
                {
                    Width = (e.X - _mouseDownLocation.X)
                                   + _oldSize.Width;
                }
            }
            else if (_nearTop)
            {
                Height -= (e.Y - _mouseDownLocation.Y);
                if (Height > 2 * Range) Top += (e.Y - _mouseDownLocation.Y);
            }
            else if (_nearBottom)
            {
                Height = (e.Y - _mouseDownLocation.Y)
                           + _oldSize.Height;
            }

            if (Width < 2 * Range) Width = 2 * Range;
            if (Height < 2 * Range) Height = 2 * Range;
        }

        private void Moving(object sender, MouseEventArgs e)
        {
            Left += e.X - _mouseDownLocation.X;
            Top += e.Y - _mouseDownLocation.Y;
        }
    }
}