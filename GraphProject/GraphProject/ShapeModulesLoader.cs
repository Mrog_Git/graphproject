﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Reflection;
using System.Windows.Forms;
using GraphProject.Interfaces;
using System.Linq;
using System.Security.Cryptography;
using System.Xml.Serialization;

namespace GraphProject
{
    internal sealed class ShapeModulesLoader
    {
        private readonly string _path;
        private readonly HashSet<Type> _shapeTypes;
        private readonly Configuration _cfg;

        public ShapeModulesLoader(string path, Configuration cfg)
        {
            _path = path;
            _cfg = cfg;
            _shapeTypes = new HashSet<Type>();

            string[] libNames = Directory.GetFiles(_path, "*"+_cfg.FormatOfExportShapesFiles);
            foreach (var lib in libNames) 
            {
                try
                {
                    if (!File.Exists(lib+@".sign")) throw new Exception("Module have not got signature!");
                    if (!VerifySignedHash(lib)) throw new Exception("Modure does not match the signature.");
                    
                    var asm = Assembly.LoadFile(lib);
                    var types = asm.ExportedTypes;
                    foreach (var t in types)
                    {
                        if (t.IsSubclassOf(typeof(Shape)) && _shapeTypes.All(ty => ty.FullName != t.FullName))
                            _shapeTypes.Add(t);
                    }
                }
                catch (Exception e)
                {
                    MessageBox.Show(@"Error while loading "+path+lib+@" : "+e.Message, @"Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                
            }
        }

        private static bool VerifySignedHash(string libPath)
        {
            RSAParameters key = new RSAParameters()
            {
                Exponent = new byte[] { 1, 0, 1, },
                Modulus = new byte[] { 227, 129, 110, 107, 174, 31, 43, 4, 70, 175, 171, 255, 245, 154, 104, 17, 191, 226, 191, 5, 242, 116, 147, 44, 82, 175, 68, 97, 236, 235, 202, 239, 128, 218, 74, 148, 205, 241, 235, 228, 4, 141, 8, 129, 20, 158, 231, 138, 226, 217, 57, 76, 86, 35, 9, 172, 22, 251, 6, 204, 158, 3, 72, 59, 252, 43, 209, 204, 43, 187, 2, 140, 212, 54, 225, 61, 235, 181, 239, 159, 5, 48, 7, 197, 172, 165, 210, 140, 90, 171, 33, 51, 48, 92, 58, 237, 184, 152, 227, 191, 57, 25, 154, 15, 220, 49, 157, 106, 43, 15, 78, 249, 236, 4, 216, 47, 107, 191, 78, 56, 138, 108, 201, 243, 172, 238, 87, 123 },
            };

            try
            {
                RSACryptoServiceProvider rsaProvider = new RSACryptoServiceProvider();
                rsaProvider.ImportParameters(key);

                byte[] dataToVerify;
                byte[] signedData;
                using (var reader = new FileStream(libPath, FileMode.Open, FileAccess.Read))
                {
                    dataToVerify = new byte[reader.Length];
                    reader.Read(dataToVerify, 0, (int)reader.Length);
                }
                using (var reader = new FileStream(libPath+@".sign", FileMode.Open, FileAccess.Read))
                {
                    signedData = new byte[reader.Length];
                    reader.Read(signedData, 0, (int)reader.Length);
                }
                return rsaProvider.VerifyData(dataToVerify, new SHA1CryptoServiceProvider(), signedData);

            }
            catch (CryptographicException e)
            {
                Console.WriteLine(e.Message);

                return false;
            }
        }

        public Dictionary<Button, Type> CreateControls(Panel container)
        {
            Dictionary<Button,Type> ret = new Dictionary<Button,Type>();

            int cnt = 0;
            foreach (var t in _shapeTypes)
            {
                var attr = ((ShapeNameAttribute) t.GetCustomAttribute(typeof(ShapeNameAttribute)));
                var name = attr != null ? attr.Name : "Unknown shape";
                var b = new Button
                {
                    Text = name,
                    Top = (cnt++)*25 + 5,
                    Left = 5,
                    Height = 25,
                    Width = container.Width - 10,
                    TextAlign = ContentAlignment.TopCenter,
                    Visible = true
                };
                container.Controls.Add(b);
     
                ret.Add(b,t);
            }

            return ret;
        }

        public XmlSerializer GetXmlSerializer(Type type)
        {

            _shapeTypes.Add(typeof(Shape));
            _shapeTypes.Add(typeof(UserShape.UserShape));
            var x =  new XmlSerializer(type,_shapeTypes.ToArray());
            _shapeTypes.Remove(typeof(Shape));
            _shapeTypes.Remove(typeof(UserShape.UserShape));
            return x;
        }
    }
}
