﻿using System;
using System.Drawing;
using System.Xml.Serialization;
using GraphProject.Interfaces;

namespace GraphProject.UserShape
{
    [Serializable]
    public class UserShape : TwoDotsShape, ISelectable
    {
        [XmlIgnore]
        public UserShapeData ShapeData;
        public string Name;

        public UserShape()
        {

        }

        public UserShape(UserShapeData shapeData)
        {
            ShapeData = shapeData;
            Name = shapeData.Name;
        }
        
        public override void Draw(Graphics g)
        {
            foreach (var s in ShapeData.ShapeList)
            {
                Shape shape = GetShapeToView(s);
                shape.Draw(g);
            }
        }

        public bool Selected(Point p)
        {
            foreach (var s in ShapeData.ShapeList)
            {
                if (s is ISelectable )
                {
                    ISelectable shape = GetShapeToView(s) as ISelectable;
                    if (shape != null && shape.Selected(p)) return true;
                }
            }
            return false;
        }

        private Shape GetShapeToView(Shape templateShape)
        {
            Point ptStart, ptEnd;
            var shape = (Shape)Activator.CreateInstance(templateShape.GetType());
            if (shape is TwoDotsShape)
            {
                TwoDotsShape t = (TwoDotsShape)templateShape;
                (shape as TwoDotsShape).SetPoints(new Point(t.PtStart.X, t.PtStart.Y), new Point(t.PtEnd.X, t.PtEnd.Y));
                ptStart = t.PtStart;
                ptEnd = t.PtEnd;
                if (shape is UserShape) (shape as UserShape).ShapeData = (templateShape as UserShape).ShapeData;
            }
            else
            {
                ManyDotsShape t = (ManyDotsShape)templateShape;
                foreach (var point in t.Points)
                {
                    (shape as ManyDotsShape)?.Points.Add(new Point(point.X, point.Y));
                }
                ptStart = t.StartPointEquivalent;
                ptEnd = t.EndPointEquivalent;
            }
            shape.PenColor = PenColor;

            var heightCoef = (float)Math.Abs(PtEnd.Y - PtStart.Y) / ShapeData.Height;
            var widthCoef = (float)Math.Abs(PtEnd.X - PtStart.X) / ShapeData.Width;

            shape.Move((int)(PtStart.X + ptStart.X * (widthCoef - 1)),
                (int)(PtStart.Y + ptStart.Y * (heightCoef - 1)));
            shape.Resize((int)((ptEnd.X - ptStart.X) * widthCoef), (int)((ptEnd.Y - ptStart.Y) * heightCoef));

            return shape;
        }
    }
}
