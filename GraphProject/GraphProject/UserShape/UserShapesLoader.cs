﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
using System.Xml.Serialization;
using GraphProject.Interfaces;

namespace GraphProject.UserShape
{
    public class UserShapesLoader
    {
        private readonly List<UserShapeData> _shapeData = new List<UserShapeData>();
        private readonly List<Type> _types;
        private readonly Configuration _cfg;

        public UserShapesLoader(List<Type> shapeTypes, Configuration configuration)
        {
            _cfg = configuration;
            _types = shapeTypes;
            if (File.Exists(System.IO.Path.GetDirectoryName(Application.ExecutablePath) + @"\UserShapes"+_cfg.FormatOfCreatedShapesFiles) &&
                configuration.AllowToCreateShapes)
            {
                try
                {
                    _types.Add(typeof(Shape));
                    _types.Add(typeof(ShapeList));
                    _types.Add(typeof(UserShape));
                    _types.Add(typeof(UserShapeData));
                    var xmlSerializer = new XmlSerializer(typeof(List<UserShapeData>), _types.ToArray());
                    using (var tr = new StreamReader(System.IO.Path.GetDirectoryName(Application.ExecutablePath) + @"\UserShapes"+_cfg.FormatOfCreatedShapesFiles))
                    {
                        _shapeData = (List<UserShapeData>)xmlSerializer.Deserialize(tr);
                    }
                    _types.Remove(typeof(Shape));
                    _types.Remove(typeof(ShapeList));
                    _types.Remove(typeof(UserShape));
                    _types.Remove(typeof(UserShapeData));
                }
                catch (Exception e)
                {
                    MessageBox.Show(e.Message);
                }
            }
            
        }


        public Dictionary<Button, UserShapeData> CreateControls(Panel container)
        {
            Dictionary<Button, UserShapeData> ret = new Dictionary<Button, UserShapeData>();

            int cnt = _types.Count;
            foreach (var t in _shapeData)
            {
                var b = new Button
                {
                    Text = t.Name,
                    Top = (cnt++) * 25 + 5,
                    Left = 5,
                    Height = 25,
                    Width = container.Width - 10,
                    TextAlign = ContentAlignment.TopCenter,
                    Visible = true
                };
                container.Controls.Add(b);

                ret.Add(b, t);
            }

            return ret;
        }

        public void Save(List<UserShapeData> data)
        {
            _types.Add(typeof(Shape));
            _types.Add(typeof(UserShape));
            _types.Add(typeof(UserShapeData));
            var xmlSerializer = new XmlSerializer(typeof(List<UserShapeData>), _types.ToArray());
            using (var tr = new StreamWriter(System.IO.Path.GetDirectoryName(Application.ExecutablePath) + @"\UserShapes"+_cfg.FormatOfCreatedShapesFiles))
            {
                xmlSerializer.Serialize(tr,data);
            }
            _types.Remove(typeof(Shape));
            _types.Remove(typeof(UserShape));
            _types.Remove(typeof(UserShapeData));
        }
    }
}
