﻿using System;
using System.Drawing;

namespace GraphProject.UserShape
{
    [Serializable]
    public class UserShapeData
    {
        public UserShapeData()
        {
            ShapeList = new ShapeList();
        }

        public UserShapeData(ShapeList shapeList, string name)
        {
            ShapeList = new ShapeList(shapeList);
            Name = name;
            Point offset = ShapeList.GetVeryTopLeftPoint();
            foreach (var shape in shapeList)
            {
                shape.Move(-offset.X,-offset.Y);
            }
            Point p = ShapeList.GetVeryBottomRightPoint();
            Height = p.Y;
            Width = p.X;
        }

        public int Height;
        public int Width;
        public ShapeList ShapeList;
        public string Name;
    }
}
