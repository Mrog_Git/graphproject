﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GraphProject
{
    public partial class SettingsForm : Form
    {
        private Configuration _configuration;
        public SettingsForm(Configuration cfg)
        {
            _configuration = cfg;
            InitializeComponent();
            tbCI.Text = cfg.FormatOfCreatedShapesFiles;
            tbEI.Text = cfg.FormatOfExportShapesFiles;
            tbWidthHeight.Text = cfg.WindowHeight.ToString();
            tbWindowWidth.Text = cfg.WindowWidth.ToString();
            foreach (var c in Enum.GetValues(typeof(KnownColor)))
            {
                cbColor.Items.Add(Color.FromKnownColor((KnownColor) c).Name);
            }
            cbColor.Text = cfg.WindowColor;
            cbCreateInstruments.Checked = cfg.AllowToCreateShapes;
            cbLoadInstruments.Checked = cfg.AllowToExportShapes;
            Invalidate();
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            try
            {
                _configuration.FormatOfCreatedShapesFiles = tbCI.Text;
                _configuration.FormatOfExportShapesFiles = tbEI.Text;
                _configuration.WindowHeight = Convert.ToInt32(tbWidthHeight.Text);
                _configuration.WindowWidth = Convert.ToInt32(tbWindowWidth.Text);
                _configuration.WindowColor = cbColor.Text;
                _configuration.AllowToCreateShapes = cbCreateInstruments.Checked;
                _configuration.AllowToExportShapes = cbLoadInstruments.Checked;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
