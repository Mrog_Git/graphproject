﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using GraphProject.Interfaces;

namespace GraphProject
{
    [Serializable]
    public class ShapeList: List<Shape>
    {
        public ShapeList()
        {

        }

        public ShapeList(List<Shape> shapeList):
            base(shapeList)
        {
            
        }

        public void DrawAll(Graphics g)
        {
            foreach (Shape shape in this)
            {
                shape.Draw(g);
            }
        }

        public Point GetVeryTopLeftPoint()
        {
            var ptStartCollection = FindAll(shape => shape is TwoDotsShape)
                        .Select(s => ((TwoDotsShape)s).PtStart)
                        .Concat(
                            FindAll(shape => shape is ManyDotsShape)
                                .Select(s => ((ManyDotsShape)s).StartPointEquivalent)).ToArray();
            int minX = ptStartCollection.Select(point => point.X).Min();
            int minY = ptStartCollection.Select(point => point.Y).Min();
            return new Point(minX, minY);
        }

        public Point GetVeryBottomRightPoint()
        {
            var ptEndCollection = FindAll(shape => shape is TwoDotsShape)
                        .Select(s => ((TwoDotsShape)s).PtEnd)
                        .Concat(
                            FindAll(shape => shape is ManyDotsShape)
                                .Select(s => ((ManyDotsShape)s).EndPointEquivalent)).ToArray();
            int maxX = ptEndCollection.Select(point => point.X).Max();
            int maxY = ptEndCollection.Select(point => point.Y).Max();
            return new Point(maxX, maxY);
        }
    }
}
