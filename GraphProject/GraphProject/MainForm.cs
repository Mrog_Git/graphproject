﻿using System;
using System.Drawing;
using System.Collections.Generic;
using System.Windows.Forms;
using System.IO;
using System.Linq;
using System.Xml;
using GraphProject.Interfaces;
using GraphProject.UserShape;

namespace GraphProject
{
    public partial class MainForm : Form
    {
        private EditorPanel _editor;
        private readonly ShapeModulesLoader _shapeModulesLoader;
        private readonly UserShapesLoader _userShapesLoader;
        private readonly Configuration _configuration;

        public MainForm()
        {
            InitializeComponent();
            _configuration = Configuration.LoadConfiguration();
            _configuration.Configure(this);

            _shapeModulesLoader =
                new ShapeModulesLoader(System.IO.Path.GetDirectoryName(Application.ExecutablePath) + @"\Shapes",
                    _configuration);
            Drawer.Pen = new Pen(btnColor.BackColor);

            if (_configuration.AllowToExportShapes) Drawer.ShapeButtons = _shapeModulesLoader.CreateControls(instrumentsPanel);
            _userShapesLoader = new UserShapesLoader(Drawer.ShapeButtons.Values.ToList(),_configuration);
            if (_configuration.AllowToCreateShapes) {Drawer.UserShapeButtons = _userShapesLoader.CreateControls(instrumentsPanel);}

            foreach (Button b in Drawer.ShapeButtons.Keys.Concat(Drawer.UserShapeButtons.Keys))
            {
                b.Click += btnShape_Click;
            }
            instrumentsPanel.Invalidate();
        }

        private void Canvas_Paint(object sender, PaintEventArgs e)
        {
            Graphics g = e.Graphics;
            Drawer.ShapeList.DrawAll(g);
            if (Drawer.DrawingShape != null) Drawer.DrawingShape.Draw(g);
        }

        private void Canvas_MouseDown(object sender, MouseEventArgs e)
        {
            if (Drawer.DrawingShape != null)
            {
                if (!Drawer.IsDrawing) Drawer.IsDrawing = true;
                if (Drawer.DrawingShape is TwoDotsShape) Drawer.PtStart = new Point(e.X, e.Y);
                else
                {
                    ManyDotsShape shape = Drawer.DrawingShape as ManyDotsShape;
                    if (shape != null && shape.IsDrawing)
                    {
                        if (!Drawer.HasTempDot) shape.Points.Add(new Point(e.X, e.Y));
                        else Drawer.HasTempDot = false;
                    }
                    else
                    {
                        shape?.StartDrawing(new Point(e.X, e.Y));
                    }
                }
            }
            else if (_editor != null) EndEditing();
            else
            {
                Shape selectedShape = Drawer.SelectShape(e.Location);
                if (selectedShape != null)
                {
                    Drawer.ShapeList.Remove(selectedShape);
                    _editor = new EditorPanel(selectedShape);
                    _editor.Visible = true;
                    Canvas.Controls.Add(_editor);
                    Canvas.Invalidate();
                }
            }
        }

        private void btnShape_Click(object sender, EventArgs e)
        {
            if (_editor != null) EndEditing();
            if (Drawer.ShapeButtons.ContainsKey((Button) sender))
                Drawer.DrawingShape = (Shape) Activator.CreateInstance(Drawer.ShapeButtons[(Button) sender]);
            else
                Drawer.DrawingShape = new UserShape.UserShape(Drawer.UserShapeButtons[(Button) sender]);


            Drawer.DrawingShape.PenColor = Drawer.Pen.Color;
        }

        private void Canvas_MouseMove(object sender, MouseEventArgs e)
        {
            if (Drawer.IsDrawing)
            {
                if (Drawer.DrawingShape is TwoDotsShape)
                {
                    Drawer.PtEnd = new Point(e.X, e.Y);
                    (Drawer.DrawingShape as TwoDotsShape).SetPoints(Drawer.PtStart, Drawer.PtEnd);
                }
                else
                {
                    ManyDotsShape shape = Drawer.DrawingShape as ManyDotsShape;
                    if (Drawer.HasTempDot) shape.Points.RemoveAt(shape.Points.Count - 1);
                    shape.Points.Add(new Point(e.X, e.Y));
                    Drawer.HasTempDot = true;
                }
                Canvas.Invalidate();
            }
            
        }

        private void Canvas_MouseUp(object sender, MouseEventArgs e)
        {
            if (Drawer.IsDrawing && (Drawer.DrawingShape is TwoDotsShape))
            {
                _editor = new EditorPanel(Drawer.DrawingShape) {Visible = true};
                Canvas.Controls.Add(_editor);
                Drawer.Reset();
                Canvas.Invalidate();
            }
        }

        private void Canvas_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            if (Drawer.IsDrawing && (Drawer.DrawingShape is ManyDotsShape))
            {
                ManyDotsShape shape = Drawer.DrawingShape as ManyDotsShape;
                shape.Points.RemoveAt(shape.Points.Count - 1);
                shape.Points.RemoveAt(shape.Points.Count - 1);
                shape.EndDrawing(new Point(e.X, e.Y));
                _editor = new EditorPanel(Drawer.DrawingShape);
                _editor.Visible = true;
                Canvas.Controls.Add(_editor);
                Drawer.Reset();
                Canvas.Invalidate();
            }
        }

        private void btnColor_Click(object sender, EventArgs e)
        {
            if (colorDialog.ShowDialog() == DialogResult.OK)
            {
                btnColor.BackColor = colorDialog.Color;
            }
        }

        private void btnColor_BackColorChanged(object sender, EventArgs e)
        {
            Drawer.Pen.Color = (sender as Button).BackColor;
            if (_editor != null)
            {
                _editor.Shape.PenColor = (sender as Button).BackColor;
                _editor.Invalidate();
            }
        }

        private void EndEditing()
        {
            Shape shape = _editor.EndEditing();
            _editor.Enabled = false;
            Canvas.Controls.Remove(_editor);
            _editor.Dispose();
            _editor = null;
            Drawer.ShapeList.Add(shape);
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (_editor != null)
            {
                _editor.Enabled = false;
                Canvas.Controls.Remove(_editor);
                _editor.Dispose();
                _editor = null;
            }
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            Drawer.ShapeList = new ShapeList();
            Drawer.DrawingShape = null;
            Drawer.Reset();
            if (_editor != null)
            {
                _editor.Enabled = false;
                Canvas.Controls.Remove(_editor);
                _editor.Dispose();
                _editor = null;
            }
            Canvas.Invalidate();
        }

        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (_configuration.AllowToCreateShapes)_userShapesLoader.Save(Drawer.UserShapeButtons.Values.ToList());
            _configuration.SaveConfiguration();
        }

        private void DataSave()
        {
            try
            {
                if (saveFileDialog1.ShowDialog() != DialogResult.OK) return;
                var xmlSerializer = _shapeModulesLoader.GetXmlSerializer(typeof(ShapeList));
                using (var tw = new StreamWriter(saveFileDialog1.FileName))
                {
                    xmlSerializer.Serialize(tw, Drawer.ShapeList);
                }
            }
            catch
            {
                MessageBox.Show(@"Something wrong!", @"Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void DataLoad()
        {
            if (openFileDialog1.ShowDialog() != DialogResult.OK) return;
            var xmlSerializer = _shapeModulesLoader.GetXmlSerializer(typeof(Shape));
            var streamReader = new StringReader(openFileDialog1.FileName);
            var readerSettings = new XmlReaderSettings {IgnoreWhitespace = true};
            var xmlReader = XmlReader.Create(new XmlTextReader(streamReader.ReadToEnd()), readerSettings);
            var doc = new XmlDocument();
            xmlReader.ReadToFollowing("Shape");
            try
            {
                while (xmlReader.NodeType == XmlNodeType.Element && xmlReader.Value != "ArrayOfShape")
                {
                    if (xmlReader.Value == "Shape") { }
                    var typeName = xmlReader.GetAttribute("xsi:type");
                    XmlReader nodeReader = new XmlNodeReader(doc.ReadNode(xmlReader));
                    if (Drawer.ShapeButtons.Values.All(p => p.Name != typeName) && typeName!=nameof(UserShape.UserShape)) continue;
                    nodeReader.Read();
                    var shape = (Shape)xmlSerializer.Deserialize(nodeReader);
                    if (shape is UserShape.UserShape)
                    {
                        if (Drawer.UserShapeButtons.Values.Any(s => s.Name == ((UserShape.UserShape) shape).Name))
                            (shape as UserShape.UserShape).ShapeData = Drawer.UserShapeButtons.Values.First(s => s.Name == ((UserShape.UserShape)shape).Name);
                        else continue;
                    }
                    Drawer.ShapeList.Add(shape);
                }
                Canvas.Invalidate();
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, @"Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                xmlReader.Close();
                streamReader.Close();
            }
        }

        private void MainForm_Shown(object sender, EventArgs e)
        {
           
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            DataSave();
        }

        private void btnOpen_Click(object sender, EventArgs e)
        {
            DataLoad();
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void btnSaveInstrument_Click(object sender, EventArgs e)
        {
            if (!Drawer.ShapeList.Any()) MessageBox.Show(@"Nothing to save!");
            else
            {
                string name = Microsoft.VisualBasic.Interaction.InputBox("Input the name of a new shape.", "New Shape");
                while (Drawer.UserShapeButtons.Values.Any(s => s.Name == name))
                {
                    MessageBox.Show(@"User shape with the same name already exists! Try another.");
                    name = Microsoft.VisualBasic.Interaction.InputBox("Input the name of a new shape.", "New Shape");
                }                   
                if (name != "")
                {
                    if (Drawer.ShapeList.Any(s => s is ManyDotsShape))
                    {
                        MessageBox.Show(@"Shapes created by user can not consist many-dots shapes.");
                        return;
                    }
                    var newShape = new UserShapeData(Drawer.ShapeList,name);
                    var b = new Button
                    {
                        Text = name,
                        Top = (Drawer.ShapeButtons.Count+Drawer.UserShapeButtons.Count) * 25 + 5,
                        Left = 5,
                        Height = 25,
                        Width = instrumentsPanel.Width - 10,
                        TextAlign = ContentAlignment.TopCenter,
                        Visible = true
                    };
                    b.Click += btnShape_Click;
                    instrumentsPanel.Controls.Add(b);
                    Drawer.UserShapeButtons.Add(b,newShape);
                    instrumentsPanel.Invalidate();

                    var shape = new UserShape.UserShape(newShape)
                    {
                        Name = name,
                        PenColor = Drawer.Pen.Color,
                        PtStart = Drawer.ShapeList.GetVeryTopLeftPoint(),
                        PtEnd = Drawer.ShapeList.GetVeryBottomRightPoint()
                    };

                    Drawer.ShapeList.Clear();
                    _editor = new EditorPanel(shape) { Visible = true };
                    Canvas.Controls.Add(_editor);

                    Canvas.Invalidate();
                }
            }
        }

        private void btnSettings_Click(object sender, EventArgs e)
        {
            var settingsForm = new SettingsForm(_configuration);
            settingsForm.ShowDialog(this);
            _configuration.Configure(this);
        }
    }

    public sealed class CanvasDoubleBuff: Panel
    {
        public CanvasDoubleBuff()
        {
            DoubleBuffered = true;
        }
    }
}
