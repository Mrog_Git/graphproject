﻿using System;
using System.Drawing;

namespace GraphProject.Interfaces
{
   [Serializable]
    public abstract class TwoDotsShape: Shape
    {
        public TwoDotsShape() { }

        public virtual void SetPoints(Point start, Point end)
        {
            PtStart = new Point(Math.Min(start.X, end.X), Math.Min(start.Y, end.Y));
            PtEnd = new Point(Math.Max(start.X, end.X), Math.Max(start.Y, end.Y));
        }

        public override void Move(int x, int y)
        {
            PtStart = new Point(PtStart.X + x, PtStart.Y + y);
            PtEnd = new Point(PtEnd.X + x, PtEnd.Y + y);
        }

        public override void Resize(int width, int height)
        {
            PtEnd = new Point(PtStart.X + width, PtStart.Y + height);
        }

        public Point PtStart { get; set; }
        public Point PtEnd { get; set; }
    }
}
