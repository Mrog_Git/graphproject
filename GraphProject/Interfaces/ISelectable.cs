﻿using System.Drawing;

namespace GraphProject.Interfaces
{
    public interface ISelectable
    {
        bool Selected(Point p);
    }
}
