﻿using System;
using System.Drawing;
using System.Xml.Serialization;

namespace GraphProject.Interfaces
{
    [Serializable]
    public abstract class Shape: IEditable
    {
        [XmlIgnore]
        public Color PenColor {
            get
            {
                return ColorTranslator.FromHtml(HtmlColor);
            }
            set
            {
                HtmlColor = ColorTranslator.ToHtml(value);
            }
        }
        public string HtmlColor;
        public abstract void Draw(Graphics g);
        public abstract void Move(int deltaX, int deltaY);
        public abstract void Resize(int width, int height);
    }
}
