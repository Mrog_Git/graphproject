﻿using System;
using System.Collections.Generic;
using System.Drawing;

namespace GraphProject.Interfaces
{
    [Serializable]
    public abstract class ManyDotsShape : Shape
    {
        public ManyDotsShape()
        {
            Points = new List<Point>();
        }

        public ManyDotsShape(Point[] points)
        {
            Points = new List<Point>(points);
        }

        public void StartDrawing(Point p)
        {
            Points.Add(p);
            IsDrawing = true;
        }

        public void EndDrawing(Point p)
        {
            Points.Add(p);
            IsDrawing = false;
        }

        public bool IsDrawing { get; set; }

        public Point StartPointEquivalent
        {
            get
            {
                Point p = new Point(Points[0].X, Points[0].Y);
                foreach (Point pt in Points)
                {
                    if (pt.X <= p.X) p.X = pt.X;
                    if (pt.Y <= p.Y) p.Y = pt.Y;
                }

                return p;
            }
        }

        public Point EndPointEquivalent
        {
            get
            {
                Point p = new Point(Points[0].X, Points[0].Y);
                foreach (Point pt in Points)
                {
                    if (pt.X >= p.X) p.X = pt.X;
                    if (pt.Y >= p.Y) p.Y = pt.Y;
                }
                return p;
            }
        }

        public override void Move(int x, int y)
        {
            for (int i = 0; i < Points.Count; i++)
            {
                Points[i] = new Point(Points[i].X + x, Points[i].Y + y);
            }
        }

        public override void Resize(int width, int height)
        {
            List<Point> newPoints = new List<Point>();
            int curWidth = EndPointEquivalent.X - StartPointEquivalent.X;
            int curHeight = EndPointEquivalent.Y - StartPointEquivalent.Y;

            if (curHeight == 0 || curWidth == 0) return;

            foreach (Point p in Points)
            {
                Point newPoint = new Point();
                newPoint.X = (int)Math.Round((float)p.X / curWidth * width);
                newPoint.Y = (int)Math.Round((float)p.Y / curHeight * height);
                newPoints.Add(newPoint);
            }

            Points = newPoints;
        }
       
        public List<Point> Points { get; set; }
    }
}
