﻿using System;

namespace GraphProject.Interfaces
{
    [AttributeUsage(AttributeTargets.Class, Inherited = false)]
    public class ShapeNameAttribute: Attribute
    {
        public ShapeNameAttribute(string name)
        {
            Name = name;
        }

        public string Name { get; }
    }
}
