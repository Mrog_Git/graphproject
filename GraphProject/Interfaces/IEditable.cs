﻿namespace GraphProject.Interfaces
{
    public interface IEditable
    {
        void Move(int deltaX, int deltaY);
        void Resize(int width, int height);
    }
}
